import numpy as np
import pandas as pd
from numpy.random import choice, randint, random_sample

from app.data_utils import load_cosmic_signatures

SELECTED_SIGNATURES = ["SBS3", "SBS5", "SBS8", "SBS84"]
CONTRIBUTION_PROFILES = {
    "G1": [0.15, 0.27, 0.5, 0.08],
    "G2": [0.1, 0.18, 0.3, 0.42],
}


def make_dummy_data(
    selected_signatures=SELECTED_SIGNATURES,
    contribution_profiles=CONTRIBUTION_PROFILES,
    n_samples=10,
    n_mutations=150,
):
    df_cosmic = load_cosmic_signatures()
    df_cosmic_filtered = df_cosmic[
        df_cosmic.signature.isin(selected_signatures)
    ].pivot_table(index="mutation_type", columns="signature", values="n")

    sample_profiles = sorted(choice(list(contribution_profiles.keys()), size=n_samples))

    samples = []
    for sample in sample_profiles:
        sample_profile = np.clip(
            contribution_profiles[sample]
            + ((random_sample(len(selected_signatures)) / 5) - 0.1),
            a_min=0.05,
            a_max=1,
        )
        sample_profile = sample_profile / sample_profile.sum()

        sample_mutations = df_cosmic_filtered.mul(sample_profile, axis=1).sum(axis=1)
        sample_nmutations = n_mutations + randint(0, int(n_mutations * 0.5))

        sample_mutations = sample_mutations / sample_mutations.sum()
        sample_mutations = (sample_mutations * sample_nmutations).astype(int)

        samples.append(sample_mutations)

    df_mutations = pd.concat(samples, axis=1)
    df_mutations.columns = [f"Sample{str(i).zfill(2)}" for i in range(1, n_samples + 1)]
    df_mutations = (
        df_mutations.reset_index()
        .melt(id_vars=["mutation_type"], var_name="sample", value_name="mutations")
        .assign(
            group=lambda x: x["sample"].apply(
                lambda y: sample_profiles[int(y.replace("Sample", "")) - 1]
            )
        )
    )

    return df_mutations
