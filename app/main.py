from dash import Dash, State, html, dcc, Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas as pd

from app.data_utils import add_std_columns, get_signatures, load_cosmic_signatures
from app.plots import (
    blank_figure,
    contribution_plot,
    mutation_profile_plot,
    signature_plot,
)
from app import layouts

cosmic_signatures = load_cosmic_signatures()

url = "https://raw.githubusercontent.com/catg-umag/bcell-lymphomas-mutational-signatures/main/data/snv_list.csv"

df = pd.read_csv(url)
sample_summary = (
    df.groupby(["group", "sample", "mutation_type"]).size().reset_index(name="n")
)

mutation_summary = (
    add_std_columns(df)
    .groupby(["group", "mutation_type", "std_mutation", "std_context"])
    .size()
    .reset_index(name="n")
)

fig = mutation_profile_plot(mutation_summary)

app = Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
server = app.server

app.layout = dbc.Container(
    [
        dbc.Row(dbc.Col(html.H1("Mutational Signatures Explorer"))),
        dbc.Row(dbc.Col(html.H3("Mutational Profile"))),
        dbc.Row(dbc.Col(html.H5("By Group"))),
        dbc.Row(
            dbc.Col(
                dcc.Graph(
                    id="mutprofile-graph", figure=fig, config={"displayModeBar": False}
                )
            )
        ),
        dbc.Row(dbc.Col(html.H5("By Sample"))),
        dbc.Row(
            dbc.Col(
                dcc.Dropdown(
                    id="sample-dropdown",
                    options=[
                        {"label": i, "value": i}
                        for i in sample_summary["sample"].unique()
                    ],
                    value="CLL_01",
                ),
                width=6,
            )
        ),
        dbc.Row(
            dbc.Col(
                dcc.Graph(
                    id="sample-mutprofile-graph",
                    figure=blank_figure(),
                    config={"displayModeBar": False},
                ),
            )
        ),
        dbc.Row(dbc.Col(html.H3("Signature Extraction"))),
        dbc.Row(
            dbc.Col(
                layouts.nsignature_selector,
                width=3,
            )
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dcc.Graph(
                            id="signatures-graph",
                            figure=blank_figure(),
                            config={"displayModeBar": False},
                        ),
                        dcc.Graph(
                            id="contributions-graph",
                            figure=blank_figure(),
                            config={"displayModeBar": False},
                        ),
                    ]
                ),
            ]
        ),
    ],
)


@app.callback(
    Output("signatures-graph", "figure"),
    Output("contributions-graph", "figure"),
    State("input-nsignatures", "value"),
    Input("button-signatures", "n_clicks"),
)
def update_signatures(n_signatures: int, _) -> tuple[px.bar, px.bar]:
    signatures, contributions = get_signatures(sample_summary, n_signatures)
    signatures_fig = signature_plot(add_std_columns(signatures))
    contributions_fig = contribution_plot(contributions)

    return signatures_fig, contributions_fig


@app.callback(
    Output("sample-mutprofile-graph", "figure"),
    Input("sample-dropdown", "value"),
)
def update_sample_mutprofile(sample: str) -> px.bar:
    sample_df = add_std_columns(sample_summary.query(f"sample == '{sample}'"))
    sample_mutprofile_plot = mutation_profile_plot(sample_df)
    return sample_mutprofile_plot


if __name__ == "__main__":
    app.run_server(debug=True)
