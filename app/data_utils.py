import pandas as pd
from sklearn.decomposition import NMF
from sklearn.linear_model import LinearRegression


def add_std_columns(df: pd.DataFrame) -> pd.DataFrame:
    if "mutation_type" not in df.columns:
        df["mutation_type"] = df.apply(
            lambda row: _get_mutation_type(row["ref"], row["alt"], row["context"]),
            axis=1,
        )

    df = df.assign(
        std_mutation=lambda x: x.mutation_type.str[2:5],
        std_context=lambda x: x.mutation_type.str[0] + "." + x.mutation_type.str[-1],
    )

    return df


def get_signatures(
    df_signatures: pd.DataFrame, n_signatures: int, normalize: bool = True
) -> tuple[pd.DataFrame, pd.DataFrame]:
    mat = df_signatures.filter(items=["sample", "mutation_type", "n"]).pivot_table(
        index="mutation_type", columns="sample", values="n", fill_value=0
    )

    nmf = NMF(n_components=n_signatures, max_iter=500, random_state=42)
    W = nmf.fit_transform(mat)
    H = nmf.components_
    names = [f"S{i}" for i in range(1, n_signatures + 1)]

    signatures = (
        pd.DataFrame(W, index=mat.index, columns=names)
        .rename_axis("mutation_type", axis=1)
        .reset_index()
        .melt(id_vars="mutation_type", var_name="signature", value_name="n")
    )
    contributions = (
        pd.DataFrame(H, index=names, columns=mat.columns)
        .rename_axis("signature", axis=0)
        .reset_index()
        .melt(id_vars="signature", var_name="sample", value_name="contribution")
    )
    if normalize:
        contributions = contributions.groupby("sample", group_keys=True).apply(
            lambda x: x.assign(
                contribution=lambda y: y.contribution / y.contribution.sum()
            )
        )

    return signatures, contributions


def fit_signatures(
    df_mutations: pd.DataFrame, df_signatures: pd.DataFrame, normalize: bool = True
) -> pd.DataFrame:
    mat = df_mutations.filter(items=["sample", "mutation_type", "n"]).pivot_table(
        index="mutation_type", columns="sample", values="n", fill_value=0
    )

    reg_nnls = LinearRegression(positive=True)
    y_pred_nnls = reg_nnls.fit(df_signatures, mat).predict(df_signatures)

    print(y_pred_nnls)

    return pd.DataFrame(y_pred_nnls, index=mat.index, columns=mat.columns)


def _get_mutation_type(ref: str, alt: str, context: str) -> str:
    if ref in ("A", "G"):
        ref = _reverse_complement(ref)
        alt = _reverse_complement(alt)
        context = _reverse_complement(context)

    return f"{context[0]}{ref}>{alt}{context[-1]}"


def load_cosmic_signatures() -> pd.DataFrame:
    url = "https://cog.sanger.ac.uk/cosmic-signatures-production/documents/COSMIC_v3.3.1_SBS_GRCh38.txt"
    cosmic_df = (
        pd.read_csv(url, sep="\t")
        .rename(columns={"Type": "mutation_type"})
        .melt(id_vars="mutation_type", var_name="signature", value_name="n")
    )

    return cosmic_df


def _reverse_complement(seq: str) -> str:
    complement = {"A": "T", "C": "G", "G": "C", "T": "A", "N": "N"}
    return seq.translate(str.maketrans(complement))[::-1]
