import pandas as pd
from plotly import express as px
from plotly import graph_objects as go

MUT_PROFILE_PALETTE = [
    "#4cd7fd",
    "#545454",
    "#ee6e6c",
    "#dfd7db",
    "#c0e294",
    "#f4d8d7",
]


def mutation_profile_plot(df_mutations: pd.DataFrame) -> px.bar:
    n_groups = len(df_mutations.group.unique())

    fig = px.bar(
        df_mutations,
        x="std_context",
        y="n",
        color="std_mutation",
        color_discrete_sequence=MUT_PROFILE_PALETTE,
        facet_row="group",
        facet_col="std_mutation",
        facet_col_spacing=0.005,
        labels={
            "std_mutation": "NT change",
            "std_context": "",
            "n": "mutations",
        },
        height=200 * n_groups,
    )
    fig.for_each_annotation(lambda a: a.update(text=a.text.split("=")[-1]))
    fig.update_layout(
        plot_bgcolor="white",
        dragmode=False,
        showlegend=False,
        margin=dict(l=0, r=0, t=50, b=20),
    )
    fig.update_yaxes(gridcolor="#f0f0f0", gridwidth=0.3)
    fig.update_xaxes(tickangle=-90, tickmode="linear")

    return fig


def signature_plot(df_signatures: pd.DataFrame) -> px.bar:
    n_signatures = len(df_signatures.signature.unique())

    fig = px.bar(
        df_signatures,
        x="std_context",
        y="n",
        color="signature",
        color_discrete_sequence=px.colors.qualitative.Pastel,
        facet_row="signature",
        facet_col="std_mutation",
        facet_col_spacing=0.005,
        labels={
            "std_mutation": "NT change",
            "std_context": "",
            "n": "",
        },
        height=170 * n_signatures,
        title="Signatures",
    )
    fig.for_each_annotation(lambda a: a.update(text=a.text.split("=")[-1]))
    fig.update_layout(
        plot_bgcolor="white",
        dragmode=False,
        showlegend=False,
        margin=dict(l=0, r=0, t=50, b=20),
    )
    fig.update_yaxes(gridcolor="#f0f0f0", gridwidth=0.3)
    fig.update_xaxes(tickangle=-90, tickmode="linear")

    return fig


def contribution_plot(df_contributions: pd.DataFrame) -> px.bar:
    fig = px.bar(
        df_contributions,
        x="sample",
        y="contribution",
        color="signature",
        color_discrete_sequence=px.colors.qualitative.Pastel,
        labels={"sample": "", "contribution": ""},
        title="Contributions",
    )
    fig.update_layout(
        plot_bgcolor="white",
        dragmode=False,
        showlegend=False,
        margin=dict(l=0, r=0, t=50, b=20),
    )
    fig.update_yaxes(gridcolor="#f0f0f0", gridwidth=0.3)
    fig.update_xaxes(tickangle=-90, tickmode="linear")

    return fig


def blank_figure():
    fig = go.Figure(go.Scatter(x=[], y=[]))
    fig.update_layout(template=None)
    fig.update_xaxes(showgrid=False, showticklabels=False, zeroline=False)
    fig.update_yaxes(showgrid=False, showticklabels=False, zeroline=False)

    return fig
