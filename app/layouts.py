import dash_bootstrap_components as dbc

nsignature_selector = [
    dbc.Label("How many signatures?"),
    dbc.InputGroup(
        [
            dbc.Input(
                id="input-nsignatures",
                type="number",
                value=3,
                min=2,
                max=8,
            ),
            dbc.Button("Extract Signatures", id="button-signatures", color="primary"),
        ]
    ),
]
